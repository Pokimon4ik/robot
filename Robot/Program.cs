﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Robot
{
    class Program
    {

        public static void write()
        {
            using (StreamWriter sw = new StreamWriter("Text.txt", true, System.Text.Encoding.Default))
            {
                sw.WriteLine("Down");
            }
        }

        static void Main(string[] args)
        {
            Robot robot = new Robot();
            robot.eventdown += write;
            robot.Move();
        }
    }
}
