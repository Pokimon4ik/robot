﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Robot
{
    class Robot
    {
        public delegate void movedown();
        public event movedown eventdown;

        Random random;
        public Robot()
        {
            random = new Random();
        }

        public void Move()
        {
            while (true)
            {
                Console.ReadKey();
                int direction = random.Next(0, 4);
                switch (direction)
                {
                    case 0: Console.WriteLine("up"); break;
                    case 1: eventdown(); Console.WriteLine("down"); break;
                    case 2: Console.WriteLine("right"); break;
                    case 3: Console.WriteLine("left"); break;
                    default: Console.WriteLine("Error"); break;
                }
            }
        }

    }
}
